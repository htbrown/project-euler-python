# Project Euler Solutions

**⚠️ This repository contains spoilers for Project Euler puzzles.**

As the Project Euler website itself says: if you photocopy a crossword solution what have you achieved? This repository is aimed to show that I have completed the puzzles and is not intended to exist as a means for other people to copy solutions; it is more rewarding if you figure them out yourself.