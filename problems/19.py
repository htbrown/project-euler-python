import datetime

sundays = 0
date = datetime.date(1901, 1, 1)
while date.isoformat() <= "2000-12-31":
    if date.weekday() == 6 and date.day == 1:
        sundays += 1
    date += datetime.timedelta(days=1)

print(sundays)