import math

def factors(n):
    fac = 0
    for i in range(1, round(math.sqrt(n)) + 1):
        if n % i == 0 and n // i != i:
            fac += 2
        elif n % i == 0:
            fac += 1
        
    return fac

add = 1
n = 0

while factors(n) <= 5:
    n += add
    add += 1

print(n)