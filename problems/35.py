from math import sqrt
from itertools import permutations

primes = [2, 3, 5]

def is_prime(x):
    if x == 1: return False
    if x in [2, 3, 5]: return True
    prime = False

    iterator = 0
    while primes[iterator] <= sqrt(x):
        divider = primes[iterator]
        if x % divider == 0:
            prime = False
            break
        else:
            prime = True
        iterator += 1

    if prime: primes.append(x)
    return prime

count = 0
for n in range(1, 1000000):
    if not is_prime(n): continue
    digits = [int(i) for i in str(n)]
    circular = False
    for y in list(permutations(digits)):
        number = int("".join(str(c) for c in y))
        if not is_prime(number):
            circular = False
            break
        else:
            circular = True
    if circular:
        count += 1

print(count)