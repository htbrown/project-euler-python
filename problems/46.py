import math

primes = [2]
def get_primes(n):
    for i in range(primes[len(primes) - 1], n):
        prime = False
        for j in [p for p in primes if p <= round(math.sqrt(i))]:
            if i % j != 0:
                prime = True
            else:
                prime = False
                break
        if prime:
            primes.append(i)

for num in range(33, 2000, 2):
    get_primes(num)
    goldbach = False
    for prime in primes:
        for square in range(round(math.sqrt((num - prime) / 2))):
            if prime + pow(square, 2) == num:
                goldbach = True
                break
            else:
                goldbach = False
                
    if not goldbach:
        print(num)
        break