primes = [2, 3, 5, 7]

i = 8
while len(primes) < 10001:
    prime = False
    for j in primes:
        if i % j == 0:
            prime = False
            break
        else:
            prime = True
    if prime:
        primes.append(i)
    i += 1

print(primes)
        