limit = 354294
total = 0

for i in range(2, limit + 1):
    digits = [int(j) for j in str(i)]
    #print(digits)
    digit_powers = [j**5 for j in digits]
    #print(digit_powers)
    if sum(digit_powers) == i:
        total += i

print(total)