fib = [1, 2]
num = 3
total = 2

while num < 4000000:
    if num % 2 == 0:
        total += num
    fib.append(num)
    num = fib[len(fib) - 1] + fib[len(fib) - 2]
    print(fib, num, total)

print(total)