count = 3
last_fibs = [1, 1, 2]

while len(str(last_fibs[2])) < 1000:
    new_fib = last_fibs[1] + last_fibs[2]
    last_fibs = [last_fibs[1], last_fibs[2], new_fib]
    count += 1

print(len(str(last_fibs[1])), len(str(last_fibs[2])))
print(count)