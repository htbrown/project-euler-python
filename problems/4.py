a = 100
largest = 0

while a < 1000:
    for i in range(100, 1000):
        n = str(a * i)
        if n[:len(n) // 2] == n[len(n) // 2:][::-1] and int(n) > largest:
            largest = int(n)
    a += 1

print(largest)
