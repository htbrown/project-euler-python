largest_number = 0
largest_sequence = []

for number in range(1, 1000001):
    n = number
    sequence = [number]
    while n > 1:
        if n % 2 == 0:
            n = n // 2
            sequence.append(n)
        else:
            n = (3 * n) + 1
            sequence.append(n)
    if len(sequence) > len(largest_sequence):
        largest_sequence = sequence
        largest_number = number

print(largest_number)