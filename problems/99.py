import math
a = 11 * math.log10(2)
b = 7 * math.log10(3)
largest = (0, 0) # (log, line number)

with open('resources/99.txt') as f:
    lines = f.readlines()
    for n in range(len(lines)):
        line = lines[n]
        base, exponent = map(int, line.split(','))
        if exponent * math.log10(base) > largest[0]:
            largest = (exponent * math.log10(base), n + 1)

print(largest[1])