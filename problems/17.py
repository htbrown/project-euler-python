# this problem uses the library inflect
import inflect
engine = inflect.engine()

string = ""
for i in range(1, 1001):
    string += engine.number_to_words(i).replace(" ", "").replace("-", "").replace(",", "")

print(len(string))