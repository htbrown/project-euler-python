limit = 1499999
total = 0

from math import factorial
fact = [factorial(i) for i in range(0, 10)]

def fact_sum(n):
    s = 0
    for i in str(n):
        s += fact[int(i)]
    return s

for i in range(10, limit):
    if fact_sum(i) == i:
        total += i

print(total)    
