import math

def factors(n):
    result = []
    if str(n) in factor_cache:
        return factor_cache[str(n)]
    
    for i in range(2, round(math.sqrt(n))):
        if n % i == 0 and n // i != i:
            result.append((i, n // i)) 
        elif n % i == 0:
            result.append((i, 0))

    factor_cache[str(n)] = result
    return result

def is_abundant(n):
    if n == 0:
        return True
    if str(n) in abundant_cache:
        return abundant_cache[str(n)]
    elif sum([pair[0] + pair[1] for pair in factors(n)]) > n:
        abundant_cache[str(n)] = True
        return True
    else:
        abundant_cache[str(n)] = False
        return False

abundant_cache = {}
factor_cache = {}
total = 0

for i in range(24, 28124):
    sum_of_two = True
    for pair in factors(i):
        if not (is_abundant(pair[0]) and is_abundant(pair[1])):
            sum_of_two = False
        else:
            sum_of_two = True
            break
    if not sum_of_two:
        total += i


print(total)
