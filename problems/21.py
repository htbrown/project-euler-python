import math

def factors(n):
    result = 1
    for i in range(2, round(math.sqrt(n))):
        if n % i == 0:
            result += i
            result += n // i
    return result

total = 0

for n in range(1, 10000):
    n_factor_sum = factors(n)
    if n == factors(n_factor_sum) and n != n_factor_sum:
        total += n

print(total)