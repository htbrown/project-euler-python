def get(li, y, x):
    if 0 <= y and y < len(li) and 0 <= x and x < len(li[y]):
        return li[y][x]
    else:
        return 0

with open("./resources/11.txt") as f:
    largest = 0
    grid = [[int(i) for i in line.split(" ")] for line in f]

    dx = [1, 0, 1, -1]
    dy = [0, 1, 1, 1]

    for y in range(len(grid)):
        for x in range(len(grid[y])):
            for difference in range(4):
                p = 1
                for i in range(4):
                    p *= get(grid, y + i * dy[difference], x + i * dx[difference])
                    if largest < p: largest = p

    print(largest)

